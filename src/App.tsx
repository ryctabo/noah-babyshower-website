import { useEffect, useState } from 'react'
import InvitationPage from './pages/InvitationPage'
import NotFoundPage from './pages/NotFoundPage'
import { EVENTS } from './constants/events'

function getToken(): string | null {
  const token = window.location.search.replace('?token=', '')
  return token === '' ? null : token
}

export default function App() {
  const [id, setId] = useState<string | null>(getToken())

  useEffect(() => {
    const onLocationChange = () => {
      setId(getToken())
    }

    window.addEventListener(EVENTS.PUSHSTATE, onLocationChange)
    window.addEventListener(EVENTS.POPSTATE, onLocationChange)

    return () => {
      window.removeEventListener(EVENTS.PUSHSTATE, onLocationChange)
      window.removeEventListener(EVENTS.POPSTATE, onLocationChange)
    }
  }, [])

  return (
    <div className='flex flex-col'>
      <div className='flex-grow w-full mx-auto'>
        {id !== null ? <InvitationPage invitationId={id} /> : <NotFoundPage />}
      </div>
      <footer className='bg-orange-200 text-center text-xs py-2 text-yellow-800 [&>p>a]:font-semibold'>
        <p>
          Images provided by <a href="https://www.freepik.es/" target='_blank' rel="noreferrer">Freepik</a>
        </p>
      </footer>
    </div>
  )
}
