function BabyHeader() {
  return (
    <header>
      <h1 className='text-8xl font-serif relative flex flex-col'>
        <span className="uppercase text-teal-700/50">
          Baby
        </span>
        <span className="font-cursive -mt-10 text-7xl text-teal-900">
          Shower
        </span>
      </h1>
    </header>
  )
}

export default BabyHeader
