import React from 'react'

interface OptionButtonProps {
  children?: React.ReactNode
  onClick: () => void
  disabled?: boolean
  marked?: boolean
}

const OptionButton: React.FC<OptionButtonProps> = ({
  children,
  onClick: handleClick,
  disabled = false,
  marked = false
}) => {
  const colors = disabled
    ? `${marked ? 'border-2 bg-white/20' : 'bg-white/10'}`
    : 'bg-white/10 hover:border-2 hover:bg-white/20 focus:border-2 transition-colors'

  const classNames = `
    text-2xl uppercase font-bold w-20 h-20 ${colors}
    flex justify-center items-center rounded-full 
    border-dashed border-current
  `
  return (
    <button disabled={disabled} onClick={handleClick} className={classNames}>
      {children}
    </button>
  )
}

export default OptionButton
