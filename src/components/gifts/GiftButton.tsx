import React, { useState } from 'react'
import { TEXT } from '../../constants/text'

interface GiftButtonProps {
  children?: React.ReactNode,
  giftId: string,
  available?: number,
  marked?: boolean,
  disabled?: boolean,
  onClick: (giftId: string) => void
}

const GiftButton: React.FC<GiftButtonProps> = ({
  children,
  giftId,
  available = 0,
  marked: initialMarked = false,
  disabled: initialDisabled = false,
  onClick: onChangeSelected
}) => {
  const [marked, setMarked] = useState(initialMarked)
  const [disabled, setDisabled] = useState(available === 0 || initialDisabled)

  const colors = disabled
    ? (marked ? 'outline-2 bg-white/40 text-current w-full' : 'outline-0 text-current bg-white/30 opacity-30')
    : 'bg-white/20 text-current hover:bg-white/40 hover:outline-2 transition-colors outline-0'

  const handleClick = () => {
    if (!marked) {
      setMarked(true)
      setDisabled(true)
      onChangeSelected(giftId)
    }
  }

  return (
    <button
      onClick={handleClick}
      disabled={disabled}
      className={`
        flex flex-col justify-center items-center text-center ${colors} 
        rounded-md w-28 h-20 p-2 outline-current outline-dashed
      `}
    >
      <span className='flex-grow font-medium mb-2 leading-none flex items-center'>
        {children}
      </span>
      <span className='text-xs block w-full h-3'>
        {`${TEXT.AVAILABLE}: ${available}`}
      </span>
    </button>
  )
}

export default GiftButton
