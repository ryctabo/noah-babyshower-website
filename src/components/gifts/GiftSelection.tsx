import { useGifts } from '../../hooks/useGifts'
import { TEXT } from '../../constants/text'
import GiftButton from './GiftButton'
import GiftSelector from './GiftSelector'
import { LinkIcon } from '../../icons/LinkIcon'

interface GiftSelectionProps {
  selected?: string
  onSelected: (key: string) => void
}

export default function GiftSelection({ selected: giftSelectedId, onSelected }: GiftSelectionProps) {
  const { gifts } = useGifts()

  const isGiftSelected = giftSelectedId !== undefined

  const giftSelected = gifts.find(gift => gift.id === giftSelectedId)

  const available = giftSelected ? (giftSelected.count - giftSelected.selected) : 0

  return (
    <section className="px-4 max-w-[700px]">
      {
        !isGiftSelected
          ? (
            <GiftSelector gifts={gifts} onSelected={onSelected} />
          )
          : (
            <>
              <div className="pb-4">
                <p>{TEXT.GIFT_SELECTED}</p>
                <p className='text-red-600 font-bold text-xs'>
                  {TEXT.GIF_UNMODIFIED}
                </p>
              </div>
              <GiftButton
                giftId={giftSelectedId}
                available={available}
                marked={true}
                onClick={onSelected}
                disabled={true}
              >
                {giftSelected?.description ?? giftSelected?.name}
              </GiftButton>
              {
                giftSelected?.href &&
                <div className='flex flex-col justify-center items-center mt-4'>
                  <p className='leading-none'>{TEXT.WANNA_TO_BE}</p>
                  <a
                    className='font-bold flex gap-2 mt-2 hover:underline'
                    target='_blank' href={giftSelected.href}
                    rel="noreferrer">
                    <span>{TEXT.FOLLOWING_LINK}</span>
                    <LinkIcon />
                  </a>
                </div>
              }
            </>
          )
      }
    </section>
  )
}
