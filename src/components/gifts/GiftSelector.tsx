import GiftButton from './GiftButton'
import { TEXT } from '../../constants/text'
import { type Gift } from '../../types'

interface GiftSelectorProps {
  gifts: Gift[]
  onSelected: (key: string) => void
}

export default function GiftSelector({ gifts, onSelected }: GiftSelectorProps) {
  return (
    <>
      <p className='pb-4'>{TEXT.GIFT_SELECTOR_DESCRIPTION}</p>
      <ul className='flex flex-wrap gap-2 justify-center'>
        {
          gifts.map(({ id, name, count, selected }) => {
            return (
              <li key={id}>
                <GiftButton
                  giftId={id}
                  available={count - selected}
                  onClick={onSelected}
                >
                  {name}
                </GiftButton>
              </li>
            )
          })
        }
      </ul>
    </>
  )
}
