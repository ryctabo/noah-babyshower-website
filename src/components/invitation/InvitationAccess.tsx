import { TEXT } from '../../constants/text'
import React from 'react'

interface InvitationAccessProps {
  numberOf: number
}

const InvitationAccess: React.FC<InvitationAccessProps> = ({ numberOf }) => {
  return (
    <section>
      <p>{TEXT.NUMBER_OF_ACCESS}</p>
      <p className='font-serif text-6xl font-semibold'>{numberOf}</p>
    </section>
  )
}

export default InvitationAccess
