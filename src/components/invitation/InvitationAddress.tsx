import { TEXT } from '../../constants/text'

function InvitationAddress() {
  return (
    <section className='px-4'>
      <h3 className='uppercase -tracking-wide font-medium mb-2'>
        {TEXT.HOPE_YOU_ASSISTANCE}
      </h3>
      <p className='text-lg leading-none font-semibold'>
        {TEXT.NEIGHBORHOOD}
      </p>
      <p className='font-semibold'>{TEXT.ADDRESS}</p>
    </section>
  )
}

export default InvitationAddress
