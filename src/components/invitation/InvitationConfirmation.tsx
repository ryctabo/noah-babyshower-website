import React, { useState } from 'react'
import OptionButton from '../OptionButton'
import { TEXT } from '../../constants/text'

interface AssistanceConfirmationProps {
  message?: string
  optionSelected: 'yes' | 'no' | undefined
  onHandle: (option: 'yes' | 'no') => void
}

const InvitationConfirmation: React.FC<AssistanceConfirmationProps> = ({
  message = '¿Confirmas asistencia?',
  optionSelected,
  onHandle
}) => {
  const [option, setOption] = useState<'yes' | 'no' | undefined>(optionSelected)

  const handleClick = (option: 'yes' | 'no') => {
    setOption(option)
    onHandle(option)
  }

  return (
    <section>
      <h3>{message}</h3>
      <div className='flex justify-center items-center gap-6 mt-4'>
        <OptionButton
          onClick={() => handleClick('yes')}
          marked={option === 'yes'}
          disabled={option !== undefined}>
          {TEXT.OPTION_YES}
        </OptionButton>
        <OptionButton
          onClick={() => handleClick('no')}
          marked={option === 'no'}
          disabled={option !== undefined}>
          {TEXT.OPTION_NO}
        </OptionButton>
      </div>
    </section>
  )
}

export default InvitationConfirmation
