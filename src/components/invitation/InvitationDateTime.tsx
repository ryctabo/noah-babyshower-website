import { useState } from 'react'

function InvitationDateTime({
  day = '',
  month = '',
  dayOfMonth = '',
  year = '',
  hour = ''
}) {
  const [comingSoon] = useState<boolean>(false)
  return (
    <>
      {!comingSoon &&
        <section className="flex uppercase justify-center items-center gap-4 font-semibold">
          <div>
            <span className='border-y-2 border-y-current py-2'>
              {day}
            </span>
          </div>
          <div className='flex flex-col'>
            <span className='leading-none'>
              {month}
            </span>
            <span className='font-serif font-semibold text-5xl'>
              {dayOfMonth}
            </span>
            <span className='leading-none'>
              {year}
            </span>
          </div>
          <div>
            <span className='border-y-2 border-y-current py-2'>
              {hour}
            </span>
          </div>
        </section>
      }

      {comingSoon &&
        <section className='max-w-[200px] mx-auto'>
          <h3 className="text-xl font-bold semibold uppercase">Próximamente</h3>
          <p className='text-sm'>
            Rezamos por la salud de mi hermanito para que regrese pronto a casa
          </p>
        </section>
      }
    </>
  )
}

export default InvitationDateTime
