import { TEXT } from '../../constants/text'
import React from 'react'

interface InvitationInfoProps {
  isDemo: boolean,
  name: string
}

const InvitationGuest: React.FC<InvitationInfoProps> = ({ isDemo, name }) => {
  return (
    <section className='px-4 flex flex-col justify-center items-center relative z-50 text-green-900'>
      <p className='max-w-[31ch]'>
        {isDemo ? TEXT.DEMO_INVITATION : TEXT.INVITATION_DESCRIPTION}
      </p>
      <h3 className='text-3xl font-medium tracking-tighter bg-green-200/30 rounded-md py-1 px-2 mt-4'>
        {name}
      </h3>
    </section>
  )
}

export default InvitationGuest
