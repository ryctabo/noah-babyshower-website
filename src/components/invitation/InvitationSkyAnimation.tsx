export default function InvitationSkyAnimation() {
  return (
    <div className='py-16 -mt-10 -mb-12 flex justify-center items-center relative overflow-hidden'>
      {/* HOT AIR BALLON */}
      <div className='relative'>
        <img
          className='w-56 animate-bounce2 relative z-30'
          src="/pictures/globo-0.png"
          alt="hot air balloon 1"
        />
        <img
          className='w-24 animate-bounce2 absolute -top-8 -right-16 z-10'
          src="/pictures/globo-2.png"
          alt="hot air balloon 2"
        />
        <img
          className='w-24 animate-bounce2 absolute bottom-0 -left-16 z-10'
          src="/pictures/globo-3.png"
          alt="hot air balloon 3"
        />
      </div>
      {/* CLOUDS */}
      <img
        className='w-32 animate-cloud opacity-0 absolute top-12 right-[calc(50%-64px)] z-20'
        style={{ animationDelay: '18s' }}
        src="/pictures/g84.png"
        alt="Cloud 1"
      />
      <img
        className='w-36 animate-cloud opacity-0 absolute bottom-32 right-[calc(50%-72px)] z-20'
        style={{ animationDelay: '5s' }}
        src="/pictures/g88.png"
        alt="Cloud 2"
      />
      <img
        className='w-48 animate-cloud opacity-0 absolute bottom-0 right-[calc(50%-96px)] z-40'
        style={{ animationDelay: '15s' }}
        src="/pictures/g80.png"
        alt="Cloud 3"
      />
      <img
        className='w-24 animate-cloud opacity-0 absolute top-24 right-[calc(50%-48px)]'
        style={{ animationDelay: '8s' }}
        src="/pictures/g78.png"
        alt="Cloud 4"
      />
      <img
        className='w-28 animate-cloud opacity-0 absolute bottom-20 right-[calc(50%-56px)]'
        style={{ animationDelay: '12s' }}
        src="/pictures/g79.png"
        alt="Cloud 5"
      />
      <img
        className='w-48 animate-cloud opacity-0 absolute top-0 z-40 right-[calc(50%-96px)]'
        style={{ animationDelay: '3s' }}
        src="/pictures/g91.png"
        alt="Cloud 6"
      />
    </div>
  )
}
