import { initializeApp } from 'firebase/app'
import { child, get, getDatabase, onValue, ref, update } from 'firebase/database'
import { type Gift } from '../types'

const app = initializeApp({
  apiKey: 'AIzaSyAtErOsYbLIGm8Z4ks-uKwhL2Ew6aHku3g',
  authDomain: 'noah-babyshower.firebaseapp.com',
  databaseURL: 'https://noah-babyshower-default-rtdb.firebaseio.com',
  projectId: 'noah-babyshower',
  storageBucket: 'noah-babyshower.appspot.com',
  messagingSenderId: '389738468034',
  appId: '1:389738468034:web:7eb0cffc98ae45999ce15b',
  measurementId: 'G-39PZ73CR1R'
})

const db = getDatabase(app)

export function getInvitationById(id: string) {
  return get(child(ref(db), `invitations/${id}`))
}

export function getAllGifts(callback: (data: Gift[]) => void) {
  return onValue(ref(db, 'gifts/'), (snapshot) => {
    const data = snapshot.val() as Record<string, {
      name: string,
      description?: string,
      href?: string,
      count: number,
      selected: number
    }>
    const array: Gift[] = []
    for (const key in data) {
      if (Object.prototype.hasOwnProperty.call(data, key)) {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const element = data[key]
        array.push({
          id: key,
          name: element.name,
          description: element.description,
          href: element.href,
          count: element.count,
          selected: element.selected
        })
      }
    }
    callback(array)
  })
}

export async function pushAssistance(
  invitationId: string,
  assistance: 'yes' | 'no'
) {
  const invitationRef = ref(db, `invitations/${invitationId}`)
  return await update(invitationRef, { assistance: assistance })
}

export async function pushGiftSelected(
  invitationId: string,
  giftId: string
) {
  const response = await get(child(ref(db), `gifts/${giftId}`))
  const { name, selected } = response.val() as { name: string, selected: number }

  const invitationRef = ref(db, `invitations/${invitationId}`)
  const giftRef = ref(db, `gifts/${giftId}`)

  return Promise.all([
    update(invitationRef, { giftSelected: giftId, nameOfGiftSelected: name }),
    update(giftRef, { selected: selected + 1 })
  ])
}
