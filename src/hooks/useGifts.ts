import { useEffect, useState } from 'react'
import { getAllGifts } from '../firebase/FirebaseService'
import { type Gift } from '../types'

interface UseGiftsResult {
  gifts: Gift[]
}

export function useGifts(): UseGiftsResult {
  const [gifts, setGifts] = useState<Gift[]>([])

  /** 
   * Get all gifts from Firebase Realtime Database and subscribe
   * when data change in the database
   */
  useEffect(() => {
    const unsubscribe = getAllGifts((data: Gift[]) => {
      setGifts(data)
    })
    return () => {
      unsubscribe()
    }
  }, [])

  return { gifts }
}
