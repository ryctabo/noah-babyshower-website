import { useEffect, useState } from 'react'
import { EVENTS } from '../constants/events'

import {
  getInvitationById,
  pushAssistance,
  pushGiftSelected
} from '../firebase/FirebaseService'
import { TEXT } from '../constants/text'
import { type Invitation } from '../types'

const navigate = (href: string) => {
  window.history.pushState(null, '', href)
  const pushStateEvent = new Event(EVENTS.PUSHSTATE)
  window.dispatchEvent(pushStateEvent)
}

interface UseInvitationProps {
  invitationId: string
}

interface UseInvitationResult {
  invitation: Invitation | null
  isLoading: boolean
  error: string | null
  onConfirmed: (assistance: 'yes' | 'no') => void,
  onGiftSelected: (giftId: string) => void
}

export default function useInvitation({
  invitationId
}: UseInvitationProps): UseInvitationResult {
  const [invitation, setInvitation] = useState<Invitation | null>(null)
  const [isLoading, setLoading] = useState(true)
  const [error, setError] = useState<string | null>(null)

  const onConfirmed = (assistance: 'yes' | 'no') => {
    if (invitation !== null) {
      setInvitation({ ...invitation, assistance })
    }

    if (invitation?.immutable) {
      return
    }

    pushAssistance(invitationId, assistance).catch(handleError)
  }

  const onGiftSelected = (giftId: string) => {
    if (invitation !== null) {
      setInvitation({ ...invitation, giftSelected: giftId })
    }

    if (invitation?.immutable) {
      return
    }

    pushGiftSelected(invitationId, giftId).catch(handleError)
  }

  const handleInvitationSuccess = (
    response: {
      exists: () => boolean,
      val: () => object
    }
  ) => {
    if (!response.exists()) {
      setLoading(false)
      navigate('/')
    }
    setInvitation(response.val() as Invitation)
    setLoading(false)
  }

  const handleError = (err: Error) => {
    setLoading(false)
    setError(TEXT.ERROR)
    console.error(err)
  }

  /** Get invitation inforamtion by ID */
  useEffect(() => {
    getInvitationById(invitationId).then(
      handleInvitationSuccess,
      handleError
    )
  }, [invitationId])

  return { invitation, isLoading, error, onConfirmed, onGiftSelected }
}
