import BabyHeader from '../components/BabyHeader'
import InvitationDateTime from '../components/invitation/InvitationDateTime'
import InvitationGuest from '../components/invitation/InvitationGuest'
import InvitationAddress from '../components/invitation/InvitationAddress'
import InvitationConfirmation from '../components/invitation/InvitationConfirmation'
import InvitationSkyAnimation from '../components/invitation/InvitationSkyAnimation'
import InvitationAccess from '../components/invitation/InvitationAccess'

import { TEXT } from '../constants/text'
import useInvitation from '../hooks/useInvitation'
import GiftSelection from '../components/gifts/GiftSelection'

interface InvitationPageProps {
  invitationId: string
}

export default function InvitationPage({ invitationId }: InvitationPageProps) {

  const {
    invitation,
    isLoading,
    error,
    onConfirmed,
    onGiftSelected
  } = useInvitation({ invitationId })

  const isYesConfirmed = () => {
    return invitation?.assistance === 'yes'
  }

  return (
    <>
      <div className={`
      bg-gradient-to-b from-emerald-50 via-teal-200 via-10% to-sky-400 h-full
      mx-auto text-center py-10 min-h-screen
    `}>
        <div className='absolute top-0 right-0 w-40 h-40 bg-yellow-300 rounded-bl-full blur-xl'></div>
        <div className={`
        absolute top-0 right-0 w-28 h-28 bg-yellow-300 rounded-bl-full shadow-yellow-200 shadow-lg
      `}></div>

        <BabyHeader />

        <div className='mt-2 pt-2 border-t-[1px] border-t-teal-800/20 relative z-50 text-teal-700'>
          <p className='font-medium'>
            {isLoading ? TEXT.NO_LOADING_ASK : TEXT.NOAH_INVITE_YOU}
          </p>
          <h2 className='text-4xl font-semibold tracking-tighter'>
            {isLoading ? TEXT.NO_LOADING_TITLE : TEXT.NOAH_NAME}
          </h2>
        </div>

        <InvitationSkyAnimation />
        {
          !isLoading && !error &&
          <main className='text-sky-900 flex flex-col gap-6 relative z-50 overflow-hidden'>
            <InvitationGuest isDemo={invitation?.immutable ?? false} name={invitation?.name ?? ''} />

            <div className="relative">
              <div className='relative z-20 mt-24 flex flex-col gap-8 pb-12 text-yellow-800'>
                <InvitationAccess numberOf={invitation?.quota ?? 0} />
                <InvitationDateTime
                  day={TEXT.INVITATION_DAY}
                  month={TEXT.INVITATION_MONTH}
                  dayOfMonth={TEXT.INVITATION_DAY_OF_MONTH}
                  year={TEXT.INVITATION_YEAR}
                  hour={TEXT.INVITATION_TIME}
                />
                <InvitationConfirmation
                  optionSelected={invitation?.assistance}
                  onHandle={onConfirmed}
                />
              </div>

              <img
                className='absolute bottom-0 left-[calc(50%-290px)] w-[500px] min-w-[500px] z-10'
                src="/pictures/zafari-min.png"
                alt="Zafari with animals"
              />
            </div>
          </main>
        }
        {
          error &&
          <div className='bg-white/60 max-w-md mx-auto rounded-md p-6 text-red-700'>
            <h3 className='text-4xl font-bold mb-4'>Ops!</h3>
            <p className='font-medium'>
              {error}
            </p>
          </div>
        }
      </div>
      <div className={`
        bg-orange-200 text-yellow-800 text-center -mt-24 pt-20 pb-6 
        flex flex-col gap-4 justify-center items-center
      `}>
        {
          isYesConfirmed() &&
          <>
            <InvitationAddress />
            <GiftSelection
              selected={invitation?.giftSelected}
              onSelected={onGiftSelected}
            />
          </>
        }
        <p className='text-2xl tracking-tighter font-semibold border-t-[1px] border-current px-12 py-2 mt-3'>
          {TEXT.THANKS_YOU}
        </p>
      </div>
    </>
  )
}
