export default function NotFoundPage() {
  return (
    <main className="container mx-auto text-center py-10">
      <h1 className="text-4xl font-bold">Ops!</h1>
      <p>no hemos encontrado una invitación</p>
    </main>
  )
}
