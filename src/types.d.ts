export interface Gift {
  id: string
  name: string
  description?: string
  href?: string
  count: number
  selected: number
}

export interface Invitation {
  name: string,
  quota: number,
  assistance: 'yes' | 'no'
  giftSelected: string
  immutable: boolean
}
