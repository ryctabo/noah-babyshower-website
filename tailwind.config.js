/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      keyframes: {
        bounce2: {
          '0%, 100%': {
            transform: 'translateY(-10%)',
            'animation-timing-function': 'cubic-bezier(0.8,0,1,1)'
          },
          '50%': {
            transform: 'none',
            'animation-timing-function': 'cubic-bezier(0,0,0.2,1)'
          }
        },
        cloud: {
          '0%': {
            position: 'absolute',
            opacity: 0,
            transform: 'translateX(150%)',
            'animation-timing-function': 'ease-in-out'
          },
          '25%': {
            opacity: 1,
          },
          '75%': {
            opacity: 1,
          },
          '100%': {
            position: 'absolute',
            opacity: 0,
            transform: 'translateX(-150%)',
            'animation-timing-function': 'ease-in-out'
          }
        }
      },
      animation: {
        bounce2: 'bounce2 2s infinite',
        cloud: 'cloud 20s infinite',
      }
    },
    fontFamily: {
      sans: ['Inter', 'ui-sans-serif', 'system-ui'],
      cursive: ['Birthstone Bounce', 'cursive'],
      serif: ['ui-serif', 'Georgia']
    }
  },
  plugins: [],
}

